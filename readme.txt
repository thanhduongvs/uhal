This repository contains HAL code which I wrote for usage in my personal
projects. This is not an actively developed project.
The code implements a very minimalistic and simple HAL. Nothing fancy but
without any dependencies.

All the code is zlib licensed.
